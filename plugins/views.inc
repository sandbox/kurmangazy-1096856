<?php
// $Id$

$plugin = array(
  'form' => 'feeds_tamper_views_form',
  'callback' => 'feeds_tamper_views_callback',
  'description' => 'feeds_tamper_views_description',
  'machine_name' => 'feeds_tamper_views_machine_name',
  'name' => 'View',
  'multi' => 'loop',
  'category' => 'Other',
);

function feeds_tamper_views_form($importer, $element_key, $settings) {
  $form = array();
  $form['view'] = array(
    '#type' => 'select',
    '#title' => t('View'),
    '#default_value' => isset($settings['view']) ? $settings['view'] : '',
    '#description' => t('The view to run. The view will be passed the value and it will return the new value. The output field will be copied as is, without formatting.'),
    '#options' => array_map('feeds_tamper_views_view_name', views_get_all_views()),
  );
  return $form;
}

function feeds_tamper_views_description($settings) {
  return 'Execute view: ' . $settings['view'];
}

function feeds_tamper_views_machine_name($settings) {
  return 'view_' . $settings['view'];
}

function feeds_tamper_views_view_name($view) {
  return $view->name;
}

function feeds_tamper_views_callback($source, $item_key, $element_key, &$field, $settings) {
  $view_name = $settings['view'];

  // call views function to get the unprocessed view result
  $result = views_get_view_result($view_name, NULL, $field);

  // turn the 1st record into an array, since we don't know the field names
  $first_result = get_object_vars($result[0]);

  // return the last field (there should only be one)
  $field = array_pop($first_result);
}
